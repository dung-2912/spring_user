package com.nguyendung.foodpet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodpetApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodpetApplication.class, args);
	}

}
