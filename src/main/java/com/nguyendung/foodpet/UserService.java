package com.nguyendung.foodpet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class UserService {

	@Autowired
	private UserRepository repo;
	
	public List<tbl_users> listALL() {
		
		return repo.findAll();
	}
	
	public void save(tbl_users tbl_users) {
		repo.save(tbl_users);
	}
	
	public tbl_users get(Long id) {
		return repo.findById(id).get();
	}
	
	public void delete(Long id) {
		repo.deleteById(id);
	}
	
}
