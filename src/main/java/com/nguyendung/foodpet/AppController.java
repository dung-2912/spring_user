package com.nguyendung.foodpet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class AppController {
	@Autowired
	private UserService service;
	
	@RequestMapping("/")
	public String viewHomePage(Model model) {
		List<tbl_users> listUsers = service.listALL();
		model.addAttribute("listUsers", listUsers);	
		return "index";	
	}
	
	@RequestMapping("/new")
	public String showNewProductPage(Model model) {
	    tbl_users user = new tbl_users();
	    model.addAttribute("user", user);
	     
	    return "new_user";
	}
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveProduct(@ModelAttribute("user") tbl_users user) {
	    service.save(user);
	     
	    return "redirect:/";
	}
}
